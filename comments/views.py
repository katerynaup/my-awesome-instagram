from django.contrib.auth.decorators import login_required
from django.http.response import JsonResponse
from django.template.loader import render_to_string
from django.urls.base import reverse
from django.utils.decorators import method_decorator
from django.views.generic.edit import CreateView
from django.views.generic.list import ListView

from comments.forms import CommentForm
from comments.models import Comment
from photos.models import Photo


class CommentCreateView(CreateView):
    model = Comment
    form_class = CommentForm

    http_method_names = ['post']

    @method_decorator(login_required(login_url='login'))
    def dispatch(self, *args, **kwargs):
        return super(CommentCreateView, self).dispatch(*args, **kwargs)

    def form_valid(self, form):
        form.instance.photo = Photo.objects.get(id=self.kwargs['pk'])
        form.instance.user = self.request.user
        self.object = form.save()
        return JsonResponse({"status": "success"})

    def form_invalid(self, form):

        return JsonResponse({"errors": render_to_string("errors.html", context={'form': form})})


class CommentUsualCreateView(CreateView):
    """ Create comment without ajax on photo detail page. """
    model = Comment
    form_class = CommentForm

    @method_decorator(login_required(login_url='login'))
    def dispatch(self, *args, **kwargs):
        return super(CommentUsualCreateView, self).dispatch(*args, **kwargs)

    def form_valid(self, form):
        form.instance.photo = Photo.objects.get(id=self.kwargs['pk'])
        form.instance.user = self.request.user
        return super(CommentUsualCreateView, self).form_valid(form)

    def get_success_url(self):
        return reverse('photo_detail', kwargs={"pk": self.kwargs['pk']})


class CommentList(ListView):
    model = Comment
    template_name = 'comments.html'

    @method_decorator(login_required(login_url='login'))
    def dispatch(self, *args, **kwargs):
        return super(CommentList, self).dispatch(*args, **kwargs)

    def get_queryset(self):
        photo_id = self.kwargs['pk']
        queryset = Comment.objects.filter(photo=photo_id).order_by('-create_date')[:5]
        return queryset


