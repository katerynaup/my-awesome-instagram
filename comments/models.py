from django.contrib.auth import get_user_model
from django.db import models

from photos.models import Photo, BaseModel

User = get_user_model()


class Comment(BaseModel):
    text = models.TextField()
    user = models.ForeignKey(User, related_name="comments")
    photo = models.ForeignKey(Photo, related_name="comments")

    class Meta:
        ordering = ('-create_date',)

    def __str__(self):
        return self.text