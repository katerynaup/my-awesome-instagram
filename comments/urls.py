from django.conf.urls import url

from comments.views import CommentCreateView, CommentList, CommentUsualCreateView

urlpatterns = [
    url(r'photo/(?P<pk>[\d]+)/comment/$', CommentCreateView.as_view(), name='comment'),
    url(r'photo/(?P<pk>[\d]+)/usual_comment/$', CommentUsualCreateView.as_view(), name='usual_comment'),
    url(r'photo/(?P<pk>[\d]+)/comments/$', CommentList.as_view(), name='comments'),

]