from django.contrib import admin
from django.utils.html import format_html

from comments.models import Comment
from photos.models import Photo



class CommentAdmin(admin.ModelAdmin):
    search_fields = ['photo__description', ]

    list_display = ('user', 'photo', 'photo_view')
    list_display_links = ('photo_view', )

    def photo_view(self, obj):
        return format_html('<img src="%s" width="80px"/>' % obj.photo.image.url)

    photo_view.short_description = "Image"


admin.site.register(Comment, CommentAdmin)
