$(function () {
    function getLatestComments(commentsBlock) {
        var url = commentsBlock.data("comments-url");
        $.ajax({
            url: url,
            type: 'get',
            dataType: 'html'


        }).done(function (data) {
            var $comments = commentsBlock.find('.photo-comments');

            $($comments).html(data);

        })
            .fail(function (e) {
                console.log(e);
            })
    }

    $('.show-comments-btn').click(function () {
        var $btn = $(this);
        var $commentsBlock = $btn.parent().find('.comments-container');
        if ($btn.text() == 'Show comments') {
            getLatestComments($commentsBlock);
            $commentsBlock.toggle();
            $btn.text("Hide comments");
        }
        else {
            $commentsBlock.toggle();
            $btn.text("Show comments");
        }

        // $btn.hide();

    });

    $('.comment-form').submit(function (e) {
        e.preventDefault();
        var url = $(this).data('action');
        var $commentsBlock = $(this).parent();
        $.ajax({
            url: url,
            type: 'post',
            data: $(this).serialize()
        }).done(function (data) {

            if (data.status == 'success') {
                // cleaning the form after submission
                $('.comment-form').find('textarea').val('');
                getLatestComments($commentsBlock);
            }
            else {
                $('.comment-form').find(".form-errors").html(data.errors)
            }

        })


    });
    $('.like-dislike-btn').click(function () {
        var $imageContainer = $(this).parent();
        var url = $(this).data('like-url');
        var csrf = $('#csrf_token').find('input').val();
        $.ajax({
            url: url,
            type: 'post',
            data: {"csrfmiddlewaretoken": csrf}
        }).done(function (data) {
            console.log(data);
            if (data.liked) {
                var text = "Dislike"
            }
            else {
                text = "Like"
            }
            $imageContainer.find('.like-dislike-btn').text(text);
            $imageContainer.find('.likes-count').text(data.likes_count)

        })
    })
});