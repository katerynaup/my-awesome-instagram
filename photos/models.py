from django.contrib.auth import get_user_model
from django.db import models
from django.urls.base import reverse

User = get_user_model()


class BaseModel(models.Model):
    create_date = models.DateTimeField(auto_now_add=True)
    update_date = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True
        ordering = ["pk"]


class Photo(BaseModel):
    image = models.ImageField(upload_to='photos')
    user = models.ForeignKey(User, related_name='photos')
    likes_count = models.PositiveIntegerField(default=0)
    description = models.TextField(blank=True)

    def __str__(self):
        return self.description

    def get_absolute_url(self):
        return reverse('photo_detail', kwargs={'pk': self.id})

    def get_photo_comments(self):
        return self.comments.all()


class Like(BaseModel):
    user = models.ForeignKey(User, related_name='likes')
    photo = models.ForeignKey('Photo', related_name='likes')




