from django.conf.urls import url
from django.views.generic.base import TemplateView, RedirectView

from photos.views import UserLoginView, UserLogoutView, PhotoCreateView, PhotoDetailView, PhotoDeleteView, PhotoListView, \
    LikeView

urlpatterns = [
    url(r'photo/(?P<pk>[\d]+)/delete/$', PhotoDeleteView.as_view(), name='delete_photo'),
    url(r'feed/$', PhotoListView.as_view(), name="feed"),
    url(r'photo/(?P<pk>[\d]+)/like/$', LikeView.as_view(), name="like_dislike"),
    url(r'add-new/$', PhotoCreateView.as_view(), name="add_new"),
    url(r'photo/(?P<pk>[\d]+)/$', PhotoDetailView.as_view(), name='photo_detail'),
    url(r'login/$', UserLoginView.as_view(), name="login"),
    url(r'logout/$', UserLogoutView.as_view(), name="logout"),
    url(r'^$', RedirectView.as_view(url='/feed/'), name='main'),


]