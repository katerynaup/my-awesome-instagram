from django.contrib import admin
from django.utils.html import format_html

from photos.models import Photo, Like


class PhotoAdmin(admin.ModelAdmin):

    list_display = ('user', 'likes_count', 'description', 'photo_view')
    list_display_links = ('photo_view', 'description')

    def photo_view(self, obj):
        return format_html('<img src="%s" width="80px"/>' % obj.image.url)

    photo_view.short_description = "Image"


admin.site.register(Photo, PhotoAdmin)
admin.site.register(Like)