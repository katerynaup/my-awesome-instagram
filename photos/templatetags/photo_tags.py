from django import template


register = template.Library()


@register.inclusion_tag('templatetags/like_button.html', takes_context=True)
def show_like_button(context, photo):
    if len(photo.likes.all()):
        button = "Dislike"
    else:
        button = "Like"
    return {"button": button, "photo": photo}



