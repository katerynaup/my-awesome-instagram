from django.forms.models import ModelForm

from photos.models import Photo


class PhotoForm(ModelForm):
    class Meta:
        model = Photo
        fields = ['image', 'description']