from django.contrib.auth import get_user_model
from django.contrib.auth.decorators import login_required
from django.contrib.auth.views import LoginView, LogoutView
from django.http.response import JsonResponse
from django.urls.base import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.generic.base import View
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView
from django.views.generic.list import ListView
from django.db.models import Prefetch

from comments.forms import CommentForm
from photos.decorators import only_image_owner
from photos.forms import PhotoForm
from photos.models import Photo, Like


User = get_user_model()


class UserLoginView(LoginView):
    template_name = 'login.html'


class UserLogoutView(LogoutView):
    next_page = '/login/'


class PhotoCreateView(CreateView):
    model = Photo
    template_name = 'add_new_photo.html'
    form_class = PhotoForm

    @method_decorator(login_required(login_url='login'))
    def dispatch(self, *args, **kwargs):
        return super(PhotoCreateView, self).dispatch(*args, **kwargs)

    def form_valid(self, form):
        form.instance.user = self.request.user

        return super(PhotoCreateView, self).form_valid(form)



class PhotoDetailView(DetailView):
    model = Photo
    template_name = 'photo_detail.html'
    context_object_name = 'photo'

    @method_decorator(login_required(login_url='login'))
    def dispatch(self, *args, **kwargs):
        return super(PhotoDetailView, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(PhotoDetailView, self).get_context_data(**kwargs)
        form = CommentForm()
        context['comment_form'] = form
        context['photo_comments'] = kwargs['object'].get_photo_comments()
        return context



class PhotoDeleteView(DeleteView):
    model = Photo
    success_url = reverse_lazy('feed')
    template_name = 'photo_detail.html'

    @method_decorator(login_required(login_url='login'))
    @only_image_owner
    def dispatch(self, *args, **kwargs):
        return super(PhotoDeleteView, self).dispatch(*args, **kwargs)


class PhotoListView(ListView):
    model = Photo
    paginate_by = 10
    template_name = 'feed.html'
    context_object_name = 'photos_list'
    ordering = '-create_date'

    @method_decorator(login_required(login_url='login'))
    def dispatch(self, *args, **kwargs):
        return super(PhotoListView, self).dispatch(*args, **kwargs)

    def get_queryset(self):
        queryset = super(PhotoListView, self).get_queryset().select_related('user')
        return queryset.prefetch_related(
            Prefetch("likes", queryset=Like.objects.filter(user=self.request.user))
        )

    def get_context_data(self, **kwargs):
        context = super(PhotoListView, self).get_context_data(**kwargs)
        form = CommentForm()
        context['comment_form'] = form
        return context


class LikeView(View):

    @method_decorator(login_required(login_url='login'))
    def dispatch(self, *args, **kwargs):
        return super(LikeView, self).dispatch(*args, **kwargs)

    def post(self, request, pk):
        user = request.user
        photo = Photo.objects.get(id=pk)
        instance, created = Like.objects.get_or_create(photo=photo, user=user)
        if not created:
            instance.delete()
            liked = False
        else:
            liked = True
        photo.likes_count = Like.objects.filter(photo=photo).count()
        photo.save()
        return JsonResponse({"liked": liked, "likes_count": photo.likes_count})
