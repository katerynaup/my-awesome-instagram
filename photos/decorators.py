from django.http.response import Http404


def only_image_owner(method):
    def wrapper(view, request, *args, **kwargs):
        object = view.get_object()
        if object.user != request.user:
            raise Http404
        return method(view, request, *args, **kwargs)
    return wrapper